# Ghostish Color Theme - Change Log

## [0.3.3]

- tweak widget/notification borders and backgrounds

## [0.3.1]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.0]

- brighten contrasting panels
- dim borders
- retain v0.2.4 for those that prefer earlier style

## [0.2.4]

- update readme and screenshot

## [0.2.3]

- fix titlebar border, FG/BG in custom mode
- fix source control graph badge colors: dim scmGraph.historyItemRefColor

## [0.2.2]

- fix manifest and pub WF

## [0.2.1]

- dimmed button BG brightened slightly for better contrast

## [0.2.0]

- dimmer button BG so as not to compete visually with project names in sidebar Git
- fix manifest repo links
- retain v0.1.3 for those who prefer earlier style

## [0.1.3]

- make badge BG transparent

## [0.1.2]

- swap button.FG/BG

## [0.1.1]

- fix screenshot

## [0.1.0]

- alter terminal ansi color set for more expression
- retain prior version for those who prefer that style

## [0.0.1]

- Initial release
