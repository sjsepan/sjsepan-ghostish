#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./sjsepan-ghostish-0.3.2.vsix
npx ovsx publish sjsepan-ghostish-0.3.2.vsix --debug --pat <PAT>
