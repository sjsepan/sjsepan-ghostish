# Ghostish Theme

Ghostish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-ghostish_code.png](./images/sjsepan-ghostish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-ghostish_codium.png](./images/sjsepan-ghostish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-ghostish_codedev.png](./images/sjsepan-ghostish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-ghostish_ads.png](./images/sjsepan-ghostish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-ghostish_theia.png](./images/sjsepan-ghostish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-ghostish_positron.png](./images/sjsepan-ghostish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/9/2025
